package AerialVehicles;


import Entities.Coordinates;

public abstract class AerialVehicle {

    public static int NOT_READY_TO_FLY = 0;
    public static int READY_TO_FLY = 1;
    public static int FLYING = 2;
    public static double MAX_HOURS_WITHOUT_FIX;

    private int hoursOfFlightSinceLastFix;
    private int flightStatus;

    public AerialVehicle() {
       this.hoursOfFlightSinceLastFix = 0;
       this.flightStatus = READY_TO_FLY;
    }

    public void flyTo(Coordinates destenation) {
        if (flightStatus == READY_TO_FLY) {
            setFlightStatus(FLYING);
            System.out.println("flight to: " + destenation);
        } else
            System.out.println("Aerial Vehicle isn't ready to fly");
    }

    public void land(Coordinates destenation) {
        System.out.println("Landing on: " + destenation);
        check();
    }

    public void check() {
        if (hoursOfFlightSinceLastFix >= MAX_HOURS_WITHOUT_FIX) {
            setFlightStatus(NOT_READY_TO_FLY);
            repair();
        } else
            setFlightStatus(READY_TO_FLY);
    }

    private void repair() {
        reserHoursOfFlightSinceLastFix();
        setFlightStatus(READY_TO_FLY);
    }

    public void reserHoursOfFlightSinceLastFix() {
        this.hoursOfFlightSinceLastFix = 0;
    }

    public void setFlightStatus(int status) {
        this.flightStatus = status;
    }

    public abstract String getAircraftName();

    public abstract String getMissileExtensions();

    public abstract String getCameraExtensions();

    public abstract String getSensorExtensions();
}
