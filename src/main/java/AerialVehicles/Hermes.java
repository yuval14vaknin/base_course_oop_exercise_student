package AerialVehicles;

import Missions.BdaMission;

public class Hermes extends Catmam {

    private BdaMission bdaMission;
    private String camera;

    public Hermes() {
        super();
        this.MAX_HOURS_WITHOUT_FIX = 100;
//        this.bdaMission = new BdaMission();
        System.out.println("enter camera type:\n" +
                "1. Regular.\n" +
                "2. Thermal.\n" +
                "3. NightVision.");

        switch (in.nextInt()) {
            case 1:
                this.camera = "Regular";
                break;
            case 2:
                this.camera = "Thermal";
                break;
            case 3:
                this.camera = "NightVision";
                break;
        }
    }

    @Override
    public String getAircraftName() {
        return null;
    }

    @Override
    public String getCameraExtensions() {
        return this.camera;
    }

    @Override
    public String getMissileExtensions() {
        return null;
    }
}
