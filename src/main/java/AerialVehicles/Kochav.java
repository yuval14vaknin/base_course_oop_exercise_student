package AerialVehicles;

import Missions.AttackMission;

public class Kochav extends Hermes {

    private int amountOfMissile;
    private String kindOfMissile;
    private AttackMission attackMission;

    public Kochav() {
        super();
        System.out.println("enter missile type:\n" +
                "1. Python.\n" +
                "2. Amram\n" +
                "3. Spice250.");

        switch (in.nextInt()) {
            case 1:
                this.kindOfMissile = "Python";
                break;
            case 2:
                this.kindOfMissile = "Amram";
                break;
            case 3:
                this.kindOfMissile = "Spice250";
                break;
        }

    }

    @Override
    public String getAircraftName() {
        return "Kochav";
    }

    @Override
    public String getMissileExtensions() {
        return this.kindOfMissile + "X" + this.amountOfMissile;
    }
}
