package AerialVehicles;


import Missions.BdaMission;

public class F16 extends AttackAircraft{

    private BdaMission bdaMission;
    private String camera;

    public F16() {
        super();
//        this.bdaMission = new BdaMission();
        System.out.println("enter camera type:\n" +
                "1. Regular.\n" +
                "2. Thermal.\n" +
                "3. NightVision.");

        switch (in.nextInt()) {
            case 1:
                this.camera = "Regular";
                break;
            case 2:
                this.camera = "Thermal";
                break;
            case 3:
                this.camera = "NightVision";
                break;
        }
    }

    @Override
    public String getAircraftName() {
        return "F16";
    }

    @Override
    public String getCameraExtensions() {
        return this.camera;
    }
}
