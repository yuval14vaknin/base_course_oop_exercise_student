package AerialVehicles;

import Missions.AttackMission;

public class Haron extends Catmam {

    private AttackMission attackMission;
    private int amountOfMissile;
    private String kindOfMissile;

    public Haron() {
        super();
        this.MAX_HOURS_WITHOUT_FIX = 150;
        System.out.println("enter amount of missiles");
        this.amountOfMissile = in.nextInt();
        System.out.println("enter missile type:\n" +
                "1. Python.\n" +
                "2. Amram\n" +
                "3. Spice250.");

        switch (in.nextInt()) {
            case 1:
                this.kindOfMissile = "Python";
                break;
            case 2:
                this.kindOfMissile = "Amram";
                break;
            case 3:
                this.kindOfMissile = "Spice250";
                break;
        }
//        this.attackMission = new AttackMission();
    }

    @Override
    public String getAircraftName() {
        return null;
    }

    @Override
    public String getMissileExtensions() {
        return this.kindOfMissile + "X" + this.amountOfMissile;
    }

    @Override
    public String getCameraExtensions() {
        return null;
    }
}
