package AerialVehicles;

import Entities.Coordinates;
import Missions.IntelligenceMission;

import java.util.Scanner;

public abstract class Catmam extends AerialVehicle {

    public static Scanner in = new Scanner(System.in);

    private IntelligenceMission intelligenceMission;
    private String sensor;

    public Catmam() {
        //this.intelligenceMission = new IntelligenceMission();
        System.out.println("choose Sensor Type:\n" +
                "1. InfraRed.\n" +
                "2. Elint.");
        switch (in.nextInt()) {
            case 1:
                this.sensor = "InfraRed";
                break;
            case 2:
                this.sensor = "Elint";
                break;
        }
    }

    public String hoverOverLocation(Coordinates destenation) {
        System.out.println("Hovering over: " + destenation);
        setFlightStatus(FLYING);
        return ("Hovering over: " + destenation);
    }

    @Override
    public String getSensorExtensions() {
        return this.sensor;
    }
}
