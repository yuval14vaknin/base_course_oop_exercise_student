package AerialVehicles;

import Missions.AttackMission;

import java.util.Scanner;

public class AttackAircraft extends AerialVehicle {

    public static Scanner in = new Scanner(System.in);

    private int amountOfMissile;
    private String kindOfMissile;
    private AttackMission attackMission;

    public int getAmountOfMissile() {
        return amountOfMissile;
    }

    public void setAmountOfMissile(int amountOfMissile) {
        this.amountOfMissile = amountOfMissile;
    }

    public String getKindOfMissile() {
        return kindOfMissile;
    }

    public void setKindOfMissile(String kindOfMissile) {
        this.kindOfMissile = kindOfMissile;
    }

    public AttackMission getAttackMission() {
        return attackMission;
    }

    public void setAttackMission(AttackMission attackMission) {
        this.attackMission = attackMission;
    }

    public AttackAircraft() {
        super();
        this.MAX_HOURS_WITHOUT_FIX = 250;
        System.out.println("enter amount of missiles");
        this.amountOfMissile = in.nextInt();
        System.out.println("enter missile type:\n" +
                "1. Python.\n" +
                "2. Amram\n" +
                "3. Spice250.");

        switch (in.nextInt()) {
            case 1:
                this.kindOfMissile = "Python";
                break;
            case 2:
                this.kindOfMissile = "Amram";
                break;
            case 3:
                this.kindOfMissile = "Spice250";
                break;
        }
        //this.attackMission = new AttackMission();
    }

    @Override
    public String getAircraftName() {
        return null;
    }

    @Override
    public String getMissileExtensions() {
        return this.kindOfMissile + "X" + this.amountOfMissile;
    }

    @Override
    public String getCameraExtensions() {
        return null;
    }

    @Override
    public String getSensorExtensions() {
        return null;
    }
}
