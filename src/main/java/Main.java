import Missions.AttackMission;
import Missions.BdaMission;
import Missions.IntelligenceMission;
import Missions.Mission;

import java.util.Scanner;

public class Main {
    public static Scanner in = new Scanner(System.in);

    public static void main(String[] args) {
        Mission mission;
        System.out.println("what mission are you planning?\n" +
                "1.Attack mission.\n" +
                "2.Bda mission.\n" +
                "3.Intelligence mission.");
        switch (in.nextInt()) {
            case 1:
                mission = new AttackMission();
                break;
            case 2:
                mission = new BdaMission();
                break;
            case 3:
                mission = new IntelligenceMission();
                break;
            default:
                throw new IllegalStateException("Unexpected value: ");
        }
        
        mission.begin();
        mission.finish();
    }
}
