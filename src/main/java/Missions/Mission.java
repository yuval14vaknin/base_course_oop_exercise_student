package Missions;

import AerialVehicles.*;
import Entities.Coordinates;

import java.util.Scanner;

public abstract class Mission {

    public static Scanner in = new Scanner(System.in);

    private String pilotName;
    private Coordinates destenation;
    private AerialVehicle aircraft;

    public Mission() {
        System.out.println("enter pilot name:");
        this.pilotName = in.nextLine();
        System.out.println("for the detenation- enter two numbers, latitude first, then longtitude:");
        newDestenation(in.nextDouble(), in.nextDouble());
    }

    public void begin() {
        aircraft.flyTo(destenation);
        System.out.println("Beginning Mission!");
    }

    public void cancel() {
        aircraft.land(destenation);
        System.out.println("Abort Mission!");
    }

    public String getPilotName() {
        return pilotName;
    }

    public void setPilotName(String pilotName) {
        this.pilotName = pilotName;
    }

    public Coordinates getDestenation() {
        return destenation;
    }

    public void setDestenation(Double latitude, Double longtitude) {
        this.destenation.setLatitude(latitude);
        this.destenation.setLongitude(longtitude);
    }

    public void newDestenation(Double latitude, Double longtitude) {
        this.destenation = new Coordinates(latitude, longtitude);
    }

    public AerialVehicle getAircraft() {
        return aircraft;
    }

    public void setAircraft(AerialVehicle aircraft) {
        this.aircraft = aircraft;
    }

    public abstract void finish();

    public abstract String execute();
}
