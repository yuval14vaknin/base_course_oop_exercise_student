package Missions;

import AerialVehicles.*;

public class IntelligenceMission extends Mission {

    private String region;

    public IntelligenceMission() {
        super();
        System.out.println("select an aircraft:\n" +
                "2. F15.\n" +
                "3. Kochav.\n" +
                "4. Zik.\n" +
                "5. Eithn.\n" +
                "6. Shoval.");

        switch (in.nextInt()) {
            case 2:
                this.setAircraft(new F15());
                break;
            case 3:
                this.setAircraft(new Kochav());
                break;
            case 4:
                this.setAircraft(new Zik());
                break;
            case 5:
                this.setAircraft(new Eitan());
                break;
            case 6:
                this.setAircraft(new Shoval());
                break;
        }
        in.nextLine();
        System.out.println("enter a region:");
        this.region = in.nextLine();
    }

    @Override
    public String execute() {
        return getPilotName() + ": " + getAircraft().getAircraftName() + " " + region + " with: sensor type: " + getAircraft().getSensorExtensions() ;
    }

    @Override
    public void finish() {
        System.out.println(execute());
        getAircraft().land(getDestenation());
        System.out.println("Finish Mission!");
    }
}
