package Missions;

import AerialVehicles.*;

public class BdaMission extends Mission {

    private String objective;

    public BdaMission() {
        super();
        System.out.println("select an aircraft:\n" +
                "1. F16.\n" +
                "2. Kochav.\n" +
                "3. Zik.\n" +
                "4. Shoval.");

        switch (in.nextInt()) {
            case 1:
                this.setAircraft(new F16());
                break;
            case 2:
                this.setAircraft(new Kochav());
                break;
            case 3:
                this.setAircraft(new Zik());
                break;
            case 4:
                this.setAircraft(new Shoval());
                break;
        }
        in.nextLine();
        System.out.println("enter an objective: ");
        String o = in.nextLine();
        setObjective(o);
    }

    public void setObjective(String objective) {
        this.objective = objective;
    }

    @Override
    public String execute() {
        return (getPilotName() + ": " + getAircraft().getAircraftName() + " " + objective + " with: " + getAircraft().getCameraExtensions() + " camera") ;
    }

    @Override
    public void finish() {
        System.out.println(execute());
        getAircraft().land(getDestenation());
        System.out.println("Finish Mission!");
    }
}
